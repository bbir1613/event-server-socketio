import {
    OK, NOT_FOUND, LAST_MODIFIED, NOT_MODIFIED, BAD_REQUEST, ETAG,
    CONFLICT, METHOD_NOT_ALLOWED, NO_CONTENT, CREATED, FORBIDDEN, setIssueRes
} from './utils';
import Router from 'koa-router';
import {getLogger} from './utils';

const log = getLogger('event');

let eventsLastUpdateMillis = null;

export class EventRouter extends Router {
    constructor(props) {
        super(props);
        this.eventStore = props.eventStore;
        this.io = props.io;
        this.get('/', async(ctx) => {
            let res = ctx.response;
            let lastModified = ctx.request.get(LAST_MODIFIED);
            if (lastModified && eventsLastUpdateMillis && eventsLastUpdateMillis <= new Date(lastModified).getTime()) {
                log('search / - 304 Not Modified (the client can use the cached data)');
                res.status = NOT_MODIFIED;
            } else {
                res.body = await this.eventStore.find({user: ctx.state.user._id});
                if (!eventsLastUpdateMillis) {
                    eventsLastUpdateMillis = Date.now();
                }
                res.set({[LAST_MODIFIED]: new Date(eventsLastUpdateMillis)});
                log('search / - 200 Ok');
            }
        }).get('/:id', async(ctx) => {
            let event = await this.eventStore.findOne({_id: ctx.params.id});
            let res = ctx.response;
            if (event) {
                if (event.user == ctx.state.user._id) {
                    log('read /:id - 200 Ok');
                    this.setEventRes(res, OK, event); //200 Ok
                } else {
                    log('read /:id - 403 Forbidden');
                    setIssueRes(res, FORBIDDEN, [{error: "It's not your event"}]);
                }
            } else {
                log('read /:id - 404 Not Found (if you know the resource was deleted, then you can return 410 Gone)');
                setIssueRes(res, NOT_FOUND, [{warning: 'Event not found'}]);
            }
        }).post('/', async(ctx) => {
            let event = ctx.request.body;
            let res = ctx.response;
            if (event.text) { //validation
                event.user = ctx.state.user._id;
                await this.createEvent(ctx, res, event);
            } else {
                log(`create / - 400 Bad Request`);
                setIssueRes(res, BAD_REQUEST, [{error: 'Text is missing'}]);
            }
        }).put('/:id', async(ctx) => {
            let event = ctx.request.body;
            let id = ctx.params.id;
            let eventId = event._id;
            let res = ctx.response;
            if (eventId && eventId != id) {
                log(`update /:id - 400 Bad Request (param id and body _id should be the same)`);
                setIssueRes(res, BAD_REQUEST, [{error: 'Param id and body _id should be the same'}]);
                return;
            }
            if (!event.text) {
                log(`update /:id - 400 Bad Request (validation errors)`);
                setIssueRes(res, BAD_REQUEST, [{error: 'Text is missing'}]);
                return;
            }
            if (!eventId) {
                await this.createEvent(ctx, res, event);
            } else {
                let persistedEvent = await this.eventStore.findOne({_id: id});
                if (persistedEvent) {
                    if (persistedEvent.user != ctx.state.user._id) {
                        log('update /:id - 403 Forbidden');
                        setIssueRes(res, FORBIDDEN, [{error: "It's not your event"}]);
                        return;
                    }
                    let eventVersion = parseInt(ctx.request.get(ETAG)) || event.version;
                    if (!eventVersion) {
                        log(`update /:id - 400 Bad Request (no version specified)`);
                        setIssueRes(res, BAD_REQUEST, [{error: 'No version specified'}]); //400 Bad Request
                    } else if (eventVersion < persistedEvent.version) {
                        log(`update /:id - 409 Conflict`);
                        setIssueRes(res, CONFLICT, [{error: 'Version conflict'}]); //409 Conflict
                    } else {
                        event.version = eventVersion + 1;
                        event.updated = Date.now();
                        event.user = ctx.state.user._id;
                        let updatedCount = await this.eventStore.update({_id: id}, event);
                        eventsLastUpdateMillis = event.updated;
                        if (updatedCount == 1) {
                            this.setEventRes(res, OK, event); //200 Ok
                            this.io.to(ctx.state.user.username).emit('event/updated', event);
                        } else {
                            log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
                            setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'Event no longer exists'}]); //
                        }
                    }
                } else {
                    log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
                    setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'Event no longer exists'}]); //Method Not Allowed
                }
            }
        }).del('/:id', async(ctx) => {
            let id = ctx.params.id;
            await this.eventStore.remove({_id: id, user: ctx.state.user._id});
            this.io.to(ctx.state.user.username).emit('event/deleted', {_id: id});
            eventsLastUpdateMillis = Date.now();
            ctx.response.status = NO_CONTENT;
            log(`remove /:id - 204 No content (even if the resource was already deleted), or 200 Ok`);
        });
    }

    async createEvent(ctx, res, event) {
        event.version = 1;
        event.updated = Date.now();
        let insertedEvent = await this.eventStore.insert(event);
        eventsLastUpdateMillis = event.updated;
        this.setEventRes(res, CREATED, insertedEvent); //201 Created
        this.io.to(ctx.state.user.username).emit('event/created', insertedEvent);
    }

    setEventRes(res, status, event) {
        res.body = event;
        res.set({
            [ETAG]: event.version,
            [LAST_MODIFIED]: new Date(event.updated)
        });
        res.status = status; //200 Ok or 201 Created
    }
}
